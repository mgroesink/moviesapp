﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace MoviesApp.Tests
{
    [TestClass()]
    public class MoviesMethodsTests
    {
        [TestMethod()]
        public void CalculateAgeTest()
        {
            DateTime now = new DateTime(2020, 10, 02);
            DateTime dob = new DateTime(1958, 1, 10);
            Assert.AreEqual(62, MoviesMethods.CalculateAge(dob, now));
        }
    }
}