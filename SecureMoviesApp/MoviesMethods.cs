﻿using System;

namespace MoviesApp
{
    public static class MoviesMethods
    {
        /// <summary>
        /// Calculates the age.
        /// </summary>
        /// <param name="datetime">The datetime.</param>
        /// <returns></returns>
        public static int CalculateAge(DateTime datetime, DateTime? now = null)
        {
            if (now == null)
            {
                now = DateTime.Now;
            }
            var age = DateTime.Now.Year - datetime.Year;
            if (datetime.Month > DateTime.Now.Year)
            {
                age--;
            }
            else if (datetime.Month == DateTime.Now.Month && datetime.Day > DateTime.Now.Day)
            {
                age--;

            }
            return age;
        }
    }
}