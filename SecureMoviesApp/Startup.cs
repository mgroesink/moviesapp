﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SecureMoviesApp.Startup))]
namespace SecureMoviesApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
