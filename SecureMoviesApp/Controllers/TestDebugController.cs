﻿using SecureMoviesApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SecureMoviesApp.Controllers
{
    public class TestDebugController : Controller
    {

        // GET: TestDebug
        public ActionResult Index()
        {
            MoviesEntities ent = new MoviesEntities();

            // Stringbuilders:
            // https://docs.microsoft.com/en-us/dotnet/api/system.text.stringbuilder?view=netcore-3.1
            StringBuilder sb = new StringBuilder("<table class=\"table\">");

            // Verbatim identifier
            // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/verbatim
            string map = @"C:\TEST\TEST\AAP.TXT";
            var actors = ent.Actor.ToList();
            foreach (var actor in actors)
            {
                sb.Append("<tr>");
                sb.Append("<td>" + actor.FullName + "</td>");
                int age = Age((DateTime)actor.DoB); // Cast DoB to a normal DateTime

                // String interpolation
                // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/interpolated
                sb.Append($"<td>{age}</td>");

                sb.Append("</tr>");
            }
            sb.Append("</table>");
            ViewBag.Output = sb.ToString();
            return View();
        }

        private int Age(DateTime dob)
        {
            int age = DateTime.Now.Year - dob.Year;
            if (dob.Month > DateTime.Now.Month) age--;
            if (dob.Month == DateTime.Now.Month && dob.Day > DateTime.Now.Day) age--;
            return age;
        }
    }
}