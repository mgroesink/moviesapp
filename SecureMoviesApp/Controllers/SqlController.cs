﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SecureMoviesApp.Controllers
{
    public class SqlController : Controller
    {
        // GET: Sql
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Index(string name)
        {
            // Connection
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Server=.\\mssqlserver01;Trusted_Connection=False;Database=movies;User Id=test;Password=test;";

            // Command
            SqlCommand cmd = new SqlCommand("rskGetActorsByName", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LASTNAME", name);

            // Reader
            SqlDataReader reader;

            try
            {
                conn.Open(); // Try to open the connection
                reader = cmd.ExecuteReader();
                StringBuilder sb = new StringBuilder("<table class='table'>");
                if(!reader.HasRows)
                {
                    ViewBag.Error = "No records found";

                    return View();
                }
                while(reader.Read())
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + reader[0].ToString() + "</td>");
                    sb.Append("<td>" + reader[1].ToString() + "</td>");
                    sb.Append("<td>" + reader[2].ToString() + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                ViewBag.Output = sb.ToString();
            }
            catch (Exception ex)
            {
                ViewBag.Error = "There is a problem with the database. Connect your admin.";
            }
            return View();

        }
    }
}