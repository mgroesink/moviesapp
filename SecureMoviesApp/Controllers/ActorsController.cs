﻿using PagedList;
using SecureMoviesApp.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MoviesApp.Controllers
{
    public class ActorsController : Controller
    {
        private MoviesEntities db = new MoviesEntities();

        // GET: Actors
        public ActionResult Index(int? page, string sortOrder, string searchString, string currentFilter,
            string genderfilter)
        {
            ViewBag.CurrentSort = sortOrder;
            // if no sortoder is given set default to lastname descending
            ViewBag.LastnameSortParm = String.IsNullOrEmpty(sortOrder) ? "lastname_desc" : "";
            ViewBag.FirstnameSortParm = sortOrder == "Firstname" ? "firstname_desc" : "Firstname";
            ViewBag.GenderSortParm = sortOrder == "Gender" ? "gender_desc" : "Gender";
            ViewBag.DoBSortParm = sortOrder == "DoB" ? "dob_desc" : "DoB";
            ViewBag.DoDSortParm = sortOrder == "DoD" ? "dod_desc" : "DoD";
            ViewBag.GenderFilter = genderfilter;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var actors = from a in db.Actor
                         select a;
            if (!String.IsNullOrEmpty(searchString))
            {
                actors = actors.Where(a => a.FullName.Contains(searchString));
            }
            if (genderfilter == "Male")
            {
                actors = actors.Where(a => a.Gender == "Male");
            }
            else if (genderfilter == "Female")
            {
                actors = actors.Where(a => a.Gender == "Female");
            }
            switch (sortOrder)
            {
                case "lastname_desc":
                    actors = actors.OrderByDescending(a => a.FamilyName);
                    break;
                case "Firstname":
                    actors = actors.OrderBy(a => a.FirstName);
                    break;
                case "firstname_desc":
                    actors = actors.OrderByDescending(a => a.FirstName);
                    break;
                case "Gender":
                    actors = actors.OrderBy(a => a.Gender);
                    break;
                case "gender_desc":
                    actors = actors.OrderByDescending(a => a.Gender);
                    break;
                case "DoB":
                    actors = actors.OrderBy(a => a.DoB);
                    break;
                case "dob_desc":
                    actors = actors.OrderByDescending(a => a.DoB);
                    break;
                case "DoD":
                    actors = actors.OrderBy(a => a.DoD);
                    break;
                case "dod_desc":
                    actors = actors.OrderByDescending(a => a.DoD);
                    break;
                default:
                    actors = actors.OrderBy(s => s.FamilyName);
                    break;
            }

            var pageNumber = page ?? 1; // Set to page 1 if page is null
            var pageSize = 10; // 10 rows per page
            return View(actors.ToPagedList(pageNumber, pageSize));
        }

        // GET: Actors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actor.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        // GET: Actors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Actors/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ActorID,FirstName,FamilyName,FullName,DoB,DoD,Gender")] Actor actor)
        {
            if (ModelState.IsValid)
            {
                db.Actor.Add(actor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(actor);
        }

        // GET: Actors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actor.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        // POST: Actors/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ActorID,FirstName,FamilyName,FullName,DoB,DoD,Gender")] Actor actor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(actor);
        }

        // GET: Actors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actor.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        // POST: Actors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Actor actor = db.Actor.Find(id);
            db.Actor.Remove(actor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
