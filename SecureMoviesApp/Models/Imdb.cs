﻿namespace SecureMoviesApp.Models
{
    public class Imdb
    {
        public Imdb(string title, string url)
        {
            Title = title;
            Url = url;
        }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}