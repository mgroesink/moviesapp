﻿using MoviesApp;
using System;
using System.ComponentModel.DataAnnotations;

namespace SecureMoviesApp.Models
{

    #region Metadata
    public class FilmMetadata
    {
        public int FilmID;
        [Required]
        public string Title;
        [DataType(DataType.Date)]
        [Display(Name = "Release Date")]
        public DateTime? ReleaseDate;
        public int? DirectorID;
        public int? StudioID;
        public string Review;
        public int? CountryID;
        public int? LanguageID;
        public int? GenreID;
        public short? RunTimeMinutes;
        public int? CertificateID;
        public long? BudgetDollars;
        public long? BoxOfficeDollars;
        [Display(Name = "Oscar nominations")]
        public byte? OscarNominations;
        [Display(Name = "Oscars won")]
        public byte? OscarWins;
    }

    public class StudioMetadata
    {
        public int StudioID;
        [Display(Name = "Studio")]
        [Required]
        public string Studio1;
        [Display(Name = "Number of Movies")]
        public int NumberOfMovies;
    }

    public class ActorMetadata
    {
        public int ActorID;
        [Required]
        [Display(Name = "First Name")]
        public string FirstName;
        [Required]
        [Display(Name = "Last Name")]
        public string FamilyName;
        [Display(Name = "Full Name")]
        public string FullName;
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DoB;
        [Display(Name = "Date of Dying")]
        [DataType(DataType.Date)]
        public DateTime? DoD;
        public string Gender;
    }

    public class DirectorMetadata
    {
        public int DirectorID { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string FamilyName;
        [Display(Name = "Full Name")]
        public string FullName;
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DoB;
        [Display(Name = "Date of dying")]
        [DataType(DataType.Date)]
        public DateTime? DoD;
        public string Gender { get; set; }
    }
    public class CountryMetadata
    {
        public int CountryID;
        [Display(Name = "Country")]
        [Required]
        public string Country1;
    }

    public class GenreMetadata
    {
        public int GenreID;
        [Display(Name = "Genre")]
        public string Genre1;
    }

    public class I8aoFilmsByGenre_ResultMetadata
    {
        public string Titel;
        public string Genre;
        [Display(Name = "Oscar nominations")]
        public byte? Oscar_nominaties;
        [Display(Name = "Oscars won")]
        public byte? Gewonnen_Oscars;
    }
    #endregion

    #region Partial classes
    [MetadataType(typeof(FilmMetadata))]
    public partial class Film
    {
    }

    [MetadataType(typeof(I8aoFilmsByGenre_ResultMetadata))]
    public partial class i8aoFilmsByGenre_Result
    {
    }

    [MetadataType(typeof(ActorMetadata))]
    public partial class Actor
    {
        public int? Age
        {
            get
            {
                if (DoB == null || DoD != null) return null;
                return MoviesMethods.CalculateAge((DateTime)DoB);
            }
        }
    }

    [MetadataType(typeof(DirectorMetadata))]
    public partial class Director
    {
    }

    [MetadataType(typeof(StudioMetadata))]
    public partial class Studio
    {
    }

    [MetadataType(typeof(CountryMetadata))]
    public partial class Country
    {
    }

    [MetadataType(typeof(GenreMetadata))]
    public partial class Genre
    {
    }

    #endregion

}