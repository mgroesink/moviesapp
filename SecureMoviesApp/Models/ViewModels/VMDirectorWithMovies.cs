﻿using System;
using System.Collections.Generic;

namespace SecureMoviesApp.Models.ViewModels
{
    public class VMDirectorWithMovies
    {
        // Field
        private string name;

        // Property
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public List<Film> Films { get; set; }

        public DateTime? Birthdate { get; set; }

    }
}